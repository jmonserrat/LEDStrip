function FirstLight (ledstrip, opts) {
	opts = opts || {};
	this.ledstrip = ledstrip;
	this.ledstrip.clear();
	this.NUM_LEDS = this.ledstrip.size();
	this.direction = 1;
	this.color = opts.color || [255,0,0]; // default to red
	this.speed = opts.speed || 20; // run every Nth tick? 1 == full speed
	this.spread = opts.spread || 3; // spread N pixels on either side
	// tick counter
	this.t = 0;

	this.position = 0;

	return this;
}

FirstLight.prototype.init = function() {return this;}

FirstLight.prototype.scan = function (tick) {

	if (!(tick % this.speed == 0)) return; // speed control

	this.ledstrip.clear();

	this.ledstrip.buffer[this.position] = this.color;	
	
	this.position += this.direction;

	this.position %= this.NUM_LEDS;

	this.ledstrip.send();

	return this;
}

FirstLight.prototype.animate = function() {
	animation = requestAnimationFrame(this.animate.bind(this)); // preserve our context

	this.scan(this.t++); // calculate waves and increment tick

	this.ledstrip.send(); // update strip
}
